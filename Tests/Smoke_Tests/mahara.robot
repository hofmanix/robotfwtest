*** Settings ***
Library     Selenium2Library
Resource        ../../Settings/Mahara_Settings.txt
Resource        ../../Objects_Repository/Initial_Page_Objects.txt
Resource        ../../Keywords/Mahara_Keywords.txt
Test Setup      Open Mahara Page    ${url}      ${browser}
Test Teardown    Close Browser

*** Variables ***
${username}     student2
${password}     Testing1

*** Test Cases ***
LoginTest
    Login To Mahara     ${username}     ${password}
    Wait Until Element Is Visible       ${errorMessages}