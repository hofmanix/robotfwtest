*** Settings ***
Library         Selenium2Library
Test Teardown   Close All Browsers

*** Variables ***
${url}          https://demo.mahara.org
${browser}      gc

*** Test Cases ***
Mahara Demo
    Run Test On Browser     gc

*** Keywords ***
Prepare ${brs}
    Open Browser    ${url}  ${brs}
    Maximize Browser Window
    Capture Page Screenshot

Run Test On Browser
    [Arguments]    ${browser}
    Prepare ${browser}
    Input Text  id=login_login_username     asdf
    Input Password  id=login_login_password     fdsa
    Click Button   id=login_submit
    Wait Until Element Is Visible   id=messages
    Page Should Contain     You have not provided the correct credentials to log in. Please check your username and password are correct.